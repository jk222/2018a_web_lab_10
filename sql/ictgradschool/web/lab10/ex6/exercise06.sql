-- Answers to Exercise 6 here


DROP TABLE IF EXISTS dbtest_exercise6Movie;
DROP TABLE IF EXISTS dbtest_exercise6Person;

CREATE TABLE IF NOT EXISTS dbtest_exercise6Person (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(64),
    gender VARCHAR(64),
    yearborn INT,
    yearjoined INT,
    numberhires INT,
    PRIMARY KEY (id)
);

INSERT INTO dbtest_exercise6Person (name, gender, yearborn, yearjoined, numberhires) VALUES
    ('Peter Jackson', 'male', 1961, 1997, 17000),
    ('Jane Campion', 'female', 1954, 1980, 30000),
    ('Roger Donaldson', 'male', 1945, 1980, 12000),
    ('Temuera Morrison', 'male', 1960, 1995, 15500),
    ('Russell Crowe', 'male', 1964, 1990, 10000);



CREATE TABLE IF NOT EXISTS dbtest_exercise6Movie (
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(64),
    director VARCHAR(64),
    weeklyrate INT,
    nameofhirer INT,
    FOREIGN KEY (nameofhirer) REFERENCES dbtest_exercise6Person(id),
    PRIMARY KEY (id)

);

INSERT INTO dbtest_exercise6Movie (title, director, weeklyrate, nameofhirer) VALUES

    ('Black Panther', 'Ryan Coogler', 2, 1),
    ('Tomb Raider','Roar Uthaug', 4, 3),
    ('The Wave', 'Roar Uthaug', 6, 4);

SELECT * FROM dbtest_exercise6Person;
SELECT * FROM dbtest_exercise6Movie;



