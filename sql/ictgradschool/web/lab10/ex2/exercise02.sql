-- Answers to Exercise 2 here
DROP TABLE IF EXISTS dbtest_computerprogrammers;

CREATE TABLE IF NOT EXISTS dbtest_computerprogrammers (
    id INT NOT NULL AUTO_INCREMENT,
    username  VARCHAR(64),
    firstname  VARCHAR(64),
    lastname  VARCHAR(64),
    email  VARCHAR(64),
    PRIMARY KEY (id)
);


INSERT INTO dbtest_computerprogrammers (username, firstname, lastname, email) VALUES
    ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
    ('programmer2', 'Peter', 'Jones', 'peter@microsoft.com'),
    ('programmer3', 'Pete', 'Smith', 'pete@microsoft.com'),
    ('programmer4', 'Andrew','Peterson', 'andrew@microsoft.com'),
    ('programmer3', 'Pete', 'Sanson', 'peteSanson@microsoft.com');

