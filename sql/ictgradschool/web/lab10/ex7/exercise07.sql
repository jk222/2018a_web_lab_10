-- Answers to Exercise 7 here
DROP TABLE IF EXISTS dbtest_exercise7Articles;
DROP TABLE IF EXISTS dbtest_exercise7Comments;

CREATE TABLE IF NOT EXISTS dbtest_exercise7Articles (
    id          INT NOT NULL AUTO_INCREMENT,
    title       VARCHAR(64),
    textcontent TEXT,

    PRIMARY KEY (id)
);

INSERT INTO dbtest_exercise7Articles (title, textcontent) VALUES

    ('Article A',
     'Donec rutrum augue vitae lectus iaculis consectetur. Donec ornare aliquet augue, a volutpat neque blandit in. Aliquam imperdiet tristique nisi, vel sodales arcu maximus sit amet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla tristique ornare faucibus. Mauris consequat enim nec turpis pharetra fermentum. Pellentesque non eleifend massa. Sed scelerisque tincidunt elit nec rutrum. Curabitur et dolor in lacus porttitor consectetur vitae in tellus. Donec nunc eros, molestie a dictum ac, vulputate ut nisl.'),
    ('Article B', 'In et dolor nec nulla finibus fermentum sed non quam. Nam cursus elementum risus, non facilisis mauris luctus at. Maecenas placerat dolor orci, sed volutpat neque consequat ut. Fusce hendrerit mauris in sapien gravida rutrum. Vestibulum lectus arcu, pulvinar vel porta non, vehicula nec quam. Duis dapibus feugiat malesuada. Quisque tempus efficitur pretium. Aenean sed dolor porttitor, ultricies massa sit amet, iaculis arcu.'),
    ('Article C', 'Nunc gravida lobortis lectus quis varius. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam vel nibh sem. Etiam commodo elit nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam scelerisque justo libero, non euismod risus bibendum suscipit. Praesent est ante, scelerisque nec tortor sit amet, rhoncus ultrices nisl. Duis finibus massa vitae magna feugiat, vitae rhoncus nunc auctor. Aenean eget dui sapien. Nullam euismod mollis quam non ornare. Pellentesque sem ante, lobortis vel iaculis quis, varius ut nunc.');


CREATE TABLE IF NOT EXISTS dbtest_exercise7Comments(
    id INT NOT NULL AUTO_INCREMENT,
    commentContent TEXT,
    articleID INT,

    FOREIGN KEY (articleID) REFERENCES dbtest_exercise7Articles(id),
    PRIMARY KEY (id)

);

INSERT INTO dbtest_exercise7Comments (commentContent, articleID) VALUES

    ('Aenean ut elementum', 1),
    ('orci condimentum felis', 3);


SELECT * FROM dbtest_exercise7Articles;
SELECT * FROM dbtest_exercise7Comments;

