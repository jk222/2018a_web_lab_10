-- Answers to Exercise 3 here
DROP TABLE IF EXISTS dbtest_videorentalstatistics;

CREATE TABLE IF NOT EXISTS dbtest_videorentalstatistics (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(64),
    gender VARCHAR(64),
    yearborn INT,
    yearjoined VARCHAR(64),
    numberhires INT,
    PRIMARY KEY (id)
);

INSERT INTO dbtest_videorentalstatistics (name, gender, yearborn, yearjoined, numberhires) VALUES
    ('Peter Jackson', 'male', '1961', '1997', '17000'),
    ('Jane Campion', 'female', '1954', '1980', '30000'),
    ('Roger Donaldson', 'male', '1945', '1980', '12000'),
    ('Temuera Morrison', 'male', '1960', '1995', '15500'),
    ('Russell Crowe', 'male', '1964', '1990', '10000');












