-- Answers to Exercise 5 here
DROP TABLE IF EXISTS dbtest_exercise5;

CREATE TABLE IF NOT EXISTS dbtest_exercise5 (
    username  VARCHAR(64),
    firstname  VARCHAR(64),
    lastname  VARCHAR(64),
    email  VARCHAR(64),
    PRIMARY KEY (username)
);


INSERT INTO dbtest_exercise5 (username, firstname, lastname, email) VALUES
    ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
    ('programmer3', 'Peter', 'Jones', 'peter@microsoft.com'),
    ('programmer2', 'Pete', 'Smith', 'pete@microsoft.com'),
    ('programmer5', 'Andrew','Peterson', 'andrew@microsoft.com'),
    ('programmer4', 'Pete', 'Sanson', 'peteSanson@microsoft.com');

SELECT * FROM dbtest_exercise5;
